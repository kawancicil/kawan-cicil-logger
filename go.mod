module bitbucket.org/kawancicil/kawan-cicil-logger

go 1.13

require (
	github.com/gin-contrib/requestid v0.0.1 // indirect
	github.com/json-iterator/go v1.1.9
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.3 // indirect
	github.com/oklog/ulid v1.3.1 // indirect
	github.com/orcaman/concurrent-map v0.0.0-20190826125027-8c72a8bb44f6 // indirect
	github.com/parnurzeal/gorequest v0.2.16
	github.com/spf13/cast v1.3.1 // indirect
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.16.0
	moul.io/http2curl v1.0.0 // indirect
)
