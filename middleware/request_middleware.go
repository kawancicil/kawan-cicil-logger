package middleware

import (
	"bitbucket.org/kawancicil/kawan-cicil-logger"
	sessionV1 "bitbucket.org/kawancicil/kawan-cicil-logger/session"
	"bitbucket.org/kawancicil/kawan-cicil-logger/utils"
	"bitbucket.org/kawancicil/kawan-cicil-logger/vo"
	"bytes"
	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
)

func requestHandler(logger logger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		// - Set session to context
		// handler for logging request
		reqId := requestid.Get(c)
		if len(reqId) == 0 {
			reqId = utils.GenerateThreadId()
		}

		session := sessionV1.New(logger).
			SetThreadID(reqId).
			SetAppName("1.0").
			SetURL(c.Request.URL.String()).
			SetMethod(c.Request.Method).
			SetHeader(c.Request.Header)

		if !skipLogging(c.Request.URL.String()) {
			buf, _ := ioutil.ReadAll(c.Request.Body)
			logBody := ioutil.NopCloser(bytes.NewBuffer(buf))
			body := ioutil.NopCloser(bytes.NewBuffer(buf)) //Create body again, because body already read so we cant pass to next process

			c.Request.Body = body
			session.SetRequest(readBody(logBody))
			session.T1(readBody(logBody))
		}

		c.Set(vo.AppSession, *session)
		c.Next()
	}
}

func skipLogging(url string) bool {
	return url == "" || url == "/"
}

func readBody(reader io.Reader) string {
	buf := new(bytes.Buffer)
	_, _ = buf.ReadFrom(reader)

	s := buf.String()
	return s
}
