package middleware

import (
	"bitbucket.org/kawancicil/kawan-cicil-logger"
	"github.com/gin-gonic/gin"
)

func SetupMiddlewareLogger(app *gin.Engine, logger logger.Logger) {
	app.Use(requestHandler(logger))
	app.Use(logResponseBody)
}
