package vo

import (
	"bitbucket.org/kawancicil/kawan-cicil-logger/session"
	"github.com/gin-gonic/gin"
)

const AppSession = "App_Session"

type ApplicationContext struct {
	*gin.Context
	Session session.Session
}

func Parse(c *gin.Context) *ApplicationContext {
	data, _ := c.Get(AppSession)
	session := data.(session.Session)
	return &ApplicationContext{Context: c, Session: session}
}