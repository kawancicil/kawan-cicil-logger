package logger

import "time"

type Options struct {
	FileLocation    string        `yaml:"fileLocation"`
	FileTdrLocation string        `yaml:"fileTdrLocation"`
	FileMaxAge      time.Duration `yaml:"fileMaxAge"`
	Stdout          bool          `yaml:"stdout"`
}
